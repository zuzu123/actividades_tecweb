<?php
    include_once __DIR__.'/database.php';

    $nombre = trim($_POST['name']);
    $marca  = trim($_POST['marca']);
    $modelo =  trim($_POST['modelo']);
    $precio =  trim($_POST['precio']);
    $detalles =  trim($_POST['detalles']);
    $unidades =  trim($_POST['unidades']);
    //$imagen   =  trim($_POST['imagen']);
    $imagen   =  'img/imagen.png';
    $eliminado = 0; 
    $data = array(
        'status'  => 'error',
        'message' => 'Ya existe un producto con ese nombre'
    ); 
    if(isset($_POST['name'])) {
     // SE ASUME QUE LOS DATOS YA FUERON VALIDADOS ANTES DE ENVIARSE
        $sql = "SELECT * FROM productos WHERE nombre = '{$nombre}' AND eliminado = 0";
	    $result = $conexion->query($sql);
        
        if ($result->num_rows == 0) {
            $conexion->set_charset("utf8");
              $sql = "INSERT INTO productos VALUES (null, '{$nombre}', '{$marca}', '{$modelo}', {$precio}, '{$detalles}', {$unidades}, '{$imagen}', {$eliminado})";
            if($conexion->query($sql)){
                echo '<div class="alert alert-danger"><strong>Oh no!</strong> Nombre de producto ya existe.</div>';
                $data['status'] =  "success";
                $data['message'] =  "Producto agregado";
            } else {
                $data['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($conexion);
            }
        }
        $result->free();
        // Cierra la conexion
        $conexion->close();
    }

    // SE HACE LA CONVERSIÓN DE ARRAY A JSON
    echo json_encode($data, JSON_PRETTY_PRINT);
?>