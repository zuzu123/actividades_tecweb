<?php
    include_once __DIR__.'/database.php';

    $id = $_POST['id'];
    // SE VERIFICA HABER RECIBIDO EL ID
        // SE REALIZA LA QUERY DE BÚSQUEDA Y AL MISMO TIEMPO SE VALIDA SI HUBO RESULTADOS
        $sql = "SELECT * FROM productos WHERE id = {$id}";
        if (!$result = $conexion->query($sql) ) {
            die("Consulta fallo".mysqli_error($conexion));
        }
            // SE OBTIENEN LOS RESULTADOS
			$data =array();
    while($row=mysqli_fetch_array($result)){
        $data[]=array(
            'nombre' => $row['nombre'],
            'precio' => $row['precio'],
            'unidades' => $row['unidades'],
            'modelo' => $row['modelo'],
            'marca' => $row['marca'],
            'detalles' => $row['detalles'],
            'imagen' => $row['imagen']
        );
    $jsonstring=json_encode($data[0], JSON_PRETTY_PRINT);
    echo $jsonstring;
}
?>