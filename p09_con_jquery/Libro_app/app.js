//Buscar productos
$(document).ready(function() {
    //validar();
    listarProductos();
    let edit = false;
    $('#search').keyup(function(e) {
        if ($('#search').val()) {
            let search = $('#search').val();
            $.ajax({
                url: 'backend/product-search.php',
                type: 'POST',
                data: { search },
                success: function(response) {
                    let productos = JSON.parse(response);
                    console.log(productos);
                    let template = '';
                    let template_bar = '';
                    productos.forEach(producto => {
                        let descripcion = '';
                        descripcion += '<li>precio: ' + producto.precio + '</li>';
                        descripcion += '<li>unidades: ' + producto.unidades + '</li>';
                        descripcion += '<li>modelo: ' + producto.modelo + '</li>';
                        descripcion += '<li>marca: ' + producto.marca + '</li>';
                        descripcion += '<li>detalles: ' + producto.detalles + '</li>';
                        template += `
                        <tr productId="${producto.id}">
                            <td>${producto.id}</td>
                            <td>${producto.nombre}</td>
                            <td><ul>${descripcion}</ul></td>
                            <td>
                                <button class="product-delete btn btn-danger">
                                    Eliminar
                                </button>
                            </td>
                        </tr>
                    `;
                    })
                    productos.forEach(producto => {
                        template_bar += `<li>${producto.nombre}</il>`;

                    })

                    $('#container').html(template_bar);
                    $('#products-result').html(template);
                    $('#products').html(template);
                    // SE HACE VISIBLE LA BARRA DE ESTADO
                    document.getElementById("product-result").className = "card my-4 d-block";
                }

            })

        }
    })

     //VALIDAR EL CAMPO NOMBRE, que no exista en la base de datos
    $('#name').keyup(function() {
        var name = $(this).val();
        if (name.length > 5) {
            $("#product-result").html('checando..')
            $.ajax({
                url: 'backend/product_name.php',
                type: 'POST',
                //data: $(this).serialize(),
                data: 'name=' + $("#name").val(),
                success: function(data) {
                    //console.log(data);
                    $("#product-result").html(data)
                    document.getElementById("product-result").className = "card my-4 d-block";
                }
            });
            return false;
        } else {
            $("#product-result").html('')

        }

    });
    
    //VALIDAR CAMPOS DEL FORMULARIO
    
    $.validator.addMethod("Alfanumerico", function(value, elemento) {
        var expreg = /^([a-zA-Z0-9]){1,25}$/; //admite valores de A a Z, numeros del 0 al 9 y entre 1 a 25 caracteres
        return this.optional(elemento) || expreg.test(value);
    }, "El campo debe tener un valor alfanumérico");

    /**  $.validator.addMethod("editorial", function(value, elemento) {
        var opcion = '';
        return this.optional(elemento) || opcion.test(value);
    }, "Seleccionar una opcion");*/


    $("#product-form").validate({
        rules: {
            name: {
                required: true,
                maxlength: 100
            },
            precio: {
                required: true,
                number: true,
                min: 100
            },
            unidades: {
                required: true,
                min: 0
            },
            modelo: {
                required: true,
                Alfanumerico: true,
                maxlength: 25
            },
            editorial: {
                required: true,
                number: true,
            },
            detalles: {
                maxlength: 250,

            },
            imagen: {
                required: true,
            },
            validator: {
                required: true,
                maxlength: 7,
                minlength: 7
            }
        },
        messages: {
            name: {
                required: "Ingresar nombre del libro.",
                maxlength: "Máximo 100 caracteres"
            },
            precio: {
                required: "Ingrese una cantidad",
                min: "Un precio mayor a 99.99"
            },
            unidades: {
                required: "Ingresar un numero igual o mayor a 0",
            },
            editorial: "Seleccione una editorial",
            detalles: "Máximo 250 caracteres",
            imagen: "Ingrese una direccion de imagen",
            modelo: {
                required: "Ingrese el modelo del libro",
                maxlength: "Se admiten menos de 25 caracteres"
            }

        },
        submitHandler: function(productform) {
            $.ajax({
                url: 'backend/product-add.php',
                type: 'POST',
                //data: $(this).serialize(),
                data: $(productform).serialize(),
                success: function(response) {
                    console.log(response);
                    $("#product-result").html(response)
                    document.getElementById("product-result").className = "card my-4 d-block";
                }

            });
            listarProductos();
        }

    });
 

    /** $("#enviar").click(function() {
            if (validar()) {
                const url = edit === false ? 'backend/product-add.php' : 'backend/product-edit.php';
                //console.log(url);
                $.post(url, $("#product-form").serialize(), function(response) {
                    console.log(response);
                    let respuesta = JSON.parse(response);
                    //listarProductos();
                    let template_bar = '';
                    template_bar = `
                        <li style="list-style: none;">status: ${respuesta.status}</li>
                        <li style="list-style: none;">message: ${respuesta.message}</li>
                    `;
                    //$('#products').trigger('reset');
                    $('#product-result').html(template_bar);
                    document.getElementById("product-result").className = "card my-4 d-block";
                    listarProductos();
                });


            } else {
                console.log('No se inserto el producto')
            }
        }) */








    //eliminar productos
    $(document).on('click', '.product-delete', function() {

        if (confirm('Estas seguro de eliminar el producto?')) {
            let elemento = $(this)[0].parentElement.parentElement;
            let id = $(elemento).attr('productId');
            $.post('backend/product-delete.php', { id }, function(response) {
                //console.log(response);
                let respuesta = JSON.parse(response);
                let template_bar = '';
                template_bar += `
                            <li style="list-style: none;">status: ${respuesta.status}</li>
                            <li style="list-style: none;">message: ${respuesta.message}</li>
                        `;
                $('#product-result').html(template_bar);
                $('#container').html(template_bar);
                // SE HACE VISIBLE LA BARRA DE ESTADO
                document.getElementById("product-result").className = "card my-4 d-block";
                // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
                //document.getElementById("container").innerHTML = template_bar;
                listarProductos();
            })
        }
    })

    //editar productos
    $(document).on('click', '.product-item', function() {
        let elemento = $(this)[0].parentElement.parentElement;
        let id = $(elemento).attr('productId');
        $.post('backend/product-unic.php', { id }, function(response) {
            console.log(response);
            let producto = JSON.parse(response);
            $('#name').val(producto.nombre);
            let descripcion = `{
                "precio": ${ producto.precio },
                "unidades": ${producto.unidades},
                "modelo": "${producto.modelo}",
                "marca": "${producto.marca}",
                "detalles": "${producto.detalles}",
                "imagen": "${producto.imagen}"
                }`;
            $('#description').val(descripcion);
            $('#productId').val(id);
            edit = true;

            //$('#description').html(template);
        })
    })


    //Listar de productos
    function listarProductos() {
        $.ajax({
            url: './backend/product-list.php',
            type: 'GET',
            success: function(response) {
                let productos = JSON.parse(response);
                let template = '';

                productos.forEach(producto => {
                    // SE CREA UNA LISTA HTML CON LA DESCRIPCIÓN DEL PRODUCTO
                    let descripcion = '';
                    descripcion += '<li>precio: ' + producto.precio + '</li>';
                    descripcion += '<li>unidades: ' + producto.unidades + '</li>';
                    descripcion += '<li>modelo: ' + producto.modelo + '</li>';
                    descripcion += '<li>marca: ' + producto.marca + '</li>';
                    descripcion += '<li>detalles: ' + producto.detalles + '</li>';
                    template += `
                                             <tr productId="${producto.id}">
                                             <td>${producto.id}</td>
                                             <td>
                                             <a href="#" class="product-item">${producto.nombre}</a>
                                             </td>
                                            <td><ul>${descripcion}</ul></td>
                                              <td>
                                            <button class="product-delete btn btn-danger" >
                                            Eliminar
                                            </button>
                                            </td>
                                              </tr> `;
                });
                $('#products').html(template);
            }
        })
    }
});